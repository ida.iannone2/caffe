package com.shop.caffe.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;

    /**
     * Utility class for testing REST controllers.
     */
    public final class TestUtil {

        private static final ObjectMapper mapper = createObjectMapper();

        private TestUtil() {
        }

        private static ObjectMapper createObjectMapper() {
            ObjectMapper mapper = new ObjectMapper();
            mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            return mapper;
        }

        public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
            return mapper.writeValueAsBytes(object);
        }

        public static <T> T convertBytesToObject(byte[] byteArray, Class<T> clazz) throws IOException {
            return mapper.readValue(byteArray, clazz);
        }
    }
