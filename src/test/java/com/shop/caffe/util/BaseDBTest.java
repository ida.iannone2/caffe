package com.shop.caffe.util;

import com.shop.cafe.repository.*;
import org.junit.jupiter.api.AfterEach;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.TimeZone;

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
public class BaseDBTest {

    public abstract class BaseSpringTest {

        /**
         * MediaType for JSON
         */
        public final MediaType APPLICATION_JSON = MediaType.APPLICATION_JSON;

        @MockBean
        protected Clock clock;

        @Autowired
        protected CaffeRepository caffeRepository;
        @Autowired
        protected FoodRepository foodRepository;
        @Autowired
        protected DrinkRepository drinkRepository;
        @Autowired
        protected OrderRepository orderRepository;
        @Autowired
        protected CashDeskRepository cashDeskRepository;
        @Autowired
        protected CartItemRepository cartItemRepository;
        @Autowired
        protected CartRepository  cartRepository;

        protected void setupClock(Instant instant) {
            Mockito.when(clock.instant()).thenReturn(instant);
            Mockito.when(clock.getZone()).thenReturn( TimeZone.getTimeZone( ZoneOffset.UTC).toZoneId());
        }

        @AfterEach
        @Transactional
        void baseTearDown() {
            clearDb();
        }


        private void clearDb() {
            clearTable(caffeRepository);
            clearTable(foodRepository);
            clearTable(drinkRepository);
            clearTable(orderRepository);
            clearTable(cashDeskRepository);
            clearTable(cartItemRepository);
            clearTable(cartRepository);
        }


        private void clearTable(JpaRepository repo) {
            repo.deleteAll();
        }
    }


}
