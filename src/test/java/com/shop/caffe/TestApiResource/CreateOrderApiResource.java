package com.shop.caffe.TestApiResource;

import com.shop.cafe.domain.Order;
import com.shop.cafe.domain.enumeration.CartState;
import com.shop.cafe.repository.OrderRepository;
import com.shop.caffe.util.BaseDBTest;
import com.shop.caffe.util.TestUtil;
import com.shop.cafe.web.data.CartDto;
import com.shop.cafe.web.data.OrderDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

    @AutoConfigureMockMvc
    @SpringBootTest
    public class CreateOrderApiResource extends BaseDBTest {

        private static final String FOOD_URL = "/api/orders";
        private static final String FOOD_URL_UUID = "/api/orders/{uuid}";
        @Autowired
        protected MockMvc mockMvc;
        @Autowired
        protected OrderRepository orderRepository;

        @Test
        void shouldCreateOrderShop() throws Exception {

            // create an price
            OrderDto orderDto = createOrder();

            final MvcResult mvcResult = mockMvc.perform(post(FOOD_URL)
                    .contentType( MediaType.APPLICATION_JSON)
                    .content( TestUtil.convertObjectToJsonBytes(orderDto))
            ).andExpect(status().isOk()).andReturn();

            final OrderDto createdOrderDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), OrderDto.class);

            Assertions.assertThat(createdOrderDto.getCartuuid()).isEqualTo(orderDto.getCartuuid());
            Assertions.assertThat(createdOrderDto.getDate()).isEqualTo(orderDto.getDate());
            Assertions.assertThat(createdOrderDto.getTotalPrice()).isEqualTo(orderDto.getTotalPrice());
            Assertions.assertThat(createdOrderDto.getType()).isEqualTo(orderDto.getType());
            Assertions.assertThat(createdOrderDto.getState()).isEqualTo(orderDto.getState());

            // Order order = orderRepository.getByUuid(createdOrderDto.getUuid());
        }
        @Test
        void shouldUpdateOrderShop() throws Exception {

            // create an price
            OrderDto orderDto = createOrder();

            final MvcResult mvcResult = mockMvc.perform(post(FOOD_URL)
                    .contentType( MediaType.APPLICATION_JSON)
                    .content( TestUtil.convertObjectToJsonBytes(orderDto))
            ).andExpect(status().isOk()).andReturn();

            final OrderDto createdOrderDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), OrderDto.class);

            Assertions.assertThat(createdOrderDto.getCartuuid()).isEqualTo(orderDto.getCartuuid());
            Assertions.assertThat(createdOrderDto.getDate()).isEqualTo(orderDto.getDate());
            Assertions.assertThat(createdOrderDto.getTotalPrice()).isEqualTo(orderDto.getTotalPrice());
            Assertions.assertThat(createdOrderDto.getType()).isEqualTo(orderDto.getType());
            Assertions.assertThat(createdOrderDto.getState()).isEqualTo(orderDto.getState());
            Order order = orderRepository.getByUuid(createdOrderDto.getUuid());


            orderDto.setTotalPrice( new BigDecimal( 24.6 ) );

            final MvcResult mvcResultUpdate = mockMvc.perform(put(FOOD_URL_UUID, createdOrderDto.getUuid())
                    .contentType( MediaType.APPLICATION_JSON)
                    .content( TestUtil.convertObjectToJsonBytes(orderDto))
            ).andExpect(status().isOk()).andReturn();

            final OrderDto updateOrderDto = TestUtil.convertBytesToObject(mvcResultUpdate.getResponse().getContentAsByteArray(), OrderDto.class);

            Assertions.assertThat(updateOrderDto.getTotalPrice()).isEqualTo(orderDto.getTotalPrice());
        }
        @Test
        void shouldDeleteOrderShop() throws Exception {

            // create an price
            OrderDto orderDto = createOrder();

            final MvcResult mvcResult = mockMvc.perform(post(FOOD_URL)
                    .contentType( MediaType.APPLICATION_JSON)
                    .content( TestUtil.convertObjectToJsonBytes(orderDto))
            ).andExpect(status().isOk()).andReturn();

            final OrderDto createdOrderDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), OrderDto.class);

            Assertions.assertThat(createdOrderDto.getCartuuid()).isEqualTo(orderDto.getCartuuid());
            Assertions.assertThat(createdOrderDto.getDate()).isEqualTo(orderDto.getDate());
            Assertions.assertThat(createdOrderDto.getTotalPrice()).isEqualTo(orderDto.getTotalPrice());
            Assertions.assertThat(createdOrderDto.getType()).isEqualTo(orderDto.getType());
            Assertions.assertThat(createdOrderDto.getState()).isEqualTo(orderDto.getState());
            Order order = orderRepository.getByUuid(createdOrderDto.getUuid());

            mockMvc.perform(delete(FOOD_URL_UUID, order.getUuid())
                    .contentType( MediaType.APPLICATION_JSON)
            ).andExpect(status().isOk()).andReturn();

            Assertions.assertThat( orderRepository.getByUuid( createdOrderDto.getUuid() )).isNull();
        }
        @Test
        void shouldGetOrderShop() throws Exception {

            // create an price
            OrderDto orderDto = createOrder();

            final MvcResult mvcResult = mockMvc.perform(post(FOOD_URL)
                    .contentType( MediaType.APPLICATION_JSON)
                    .content( TestUtil.convertObjectToJsonBytes(orderDto))
            ).andExpect(status().isOk()).andReturn();

            final OrderDto createdOrderDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), OrderDto.class);

            Assertions.assertThat(createdOrderDto.getCartuuid()).isEqualTo(orderDto.getCartuuid());
            Assertions.assertThat(createdOrderDto.getDate()).isEqualTo(orderDto.getDate());
            Assertions.assertThat(createdOrderDto.getTotalPrice()).isEqualTo(orderDto.getTotalPrice());
            Assertions.assertThat(createdOrderDto.getType()).isEqualTo(orderDto.getType());
            Assertions.assertThat(createdOrderDto.getState()).isEqualTo(orderDto.getState());
            Order order = orderRepository.getByUuid(createdOrderDto.getUuid());

            final MvcResult mvcResultGet = mockMvc.perform(get(FOOD_URL_UUID, order.getUuid())
                    .contentType( MediaType.APPLICATION_JSON)
            ).andExpect(status().isOk()).andReturn();
            final OrderDto getOrderDto = TestUtil.convertBytesToObject(mvcResultGet.getResponse().getContentAsByteArray(), OrderDto.class);

            Assertions.assertThat(createdOrderDto.getCartuuid()).isEqualTo(getOrderDto.getCartuuid());
            Assertions.assertThat(createdOrderDto.getDate()).isEqualTo(getOrderDto.getDate());
            Assertions.assertThat(createdOrderDto.getTotalPrice()).isEqualTo(getOrderDto.getTotalPrice());
            Assertions.assertThat(createdOrderDto.getType()).isEqualTo(getOrderDto.getType());
            Assertions.assertThat(createdOrderDto.getState()).isEqualTo(getOrderDto.getState());
        }

        private OrderDto createOrder() throws Exception {
            OrderDto orderDto = new OrderDto();
            orderDto.setUuid( "uuid-local-test-order" );
            orderDto.setType( "DELIVERY" );
            orderDto.setDate( LocalDate.now() );
            orderDto.setTotalPrice(new BigDecimal( 23.4) );
            orderDto.setCartuuid( createCart());
            return orderDto;
        }
        private String createCart() throws Exception {

            final CartDto cartDto = createCartDto();

            final MvcResult mvcResult = mockMvc.perform(post("/cart")
                    .contentType( MediaType.APPLICATION_JSON)
                    .content( TestUtil.convertObjectToJsonBytes(cartDto)))
                    .andExpect(status().isOk())
                    .andReturn();

            CartDto cart = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), CartDto.class);
            return cart.getUuid();
        }
        private CartDto createCartDto(){
            CartDto cartDto = new CartDto();
            cartDto.setUuid( "cart-uuid" );
            cartDto.setState( CartState.CREATE);
            cartDto.setItems( Set.of());
            cartDto.setCurrency( "eur" );
            cartDto.setConsumerReference( "user" );
            cartDto.setCovered(1.5f);
            cartDto.setTotalPrice(3f);
            return cartDto;
        }

    }

