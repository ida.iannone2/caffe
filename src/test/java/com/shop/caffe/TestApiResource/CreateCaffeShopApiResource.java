package com.shop.caffe.TestApiResource;


import com.shop.cafe.domain.Caffe;
import com.shop.cafe.repository.CaffeRepository;
import com.shop.caffe.util.BaseDBTest;
import com.shop.caffe.util.TestUtil;
import com.shop.cafe.web.data.CaffeDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@AutoConfigureMockMvc
@SpringBootTest
public class CreateCaffeShopApiResource extends BaseDBTest {

    private static final String CAFFE_URL = "/api/caffe";
    private static final String CAFFE_URL_UPDATE = "/api/caffe/{uuid}";
    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected CaffeRepository caffeRepository;

    @Test
    void shouldCreateCaffeShop() throws Exception {

        // create an price
        CaffeDto caffeDto = createCaffe();

        final MvcResult mvcResult = mockMvc.perform(post(CAFFE_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(caffeDto))
        ).andExpect(status().isOk()).andReturn();

        final CaffeDto createdCaffeDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), CaffeDto.class);

        Assertions.assertThat(createdCaffeDto.getActive()).isEqualTo(caffeDto.getActive());
        Assertions.assertThat(createdCaffeDto.getAddress()).isEqualTo(caffeDto.getAddress());
        Assertions.assertThat(createdCaffeDto.getContactEmail()).isEqualTo(caffeDto.getContactEmail());
        Assertions.assertThat(createdCaffeDto.getContactPhone()).isEqualTo(caffeDto.getContactPhone());
        Assertions.assertThat(createdCaffeDto.getContactName()).isEqualTo(caffeDto.getContactName());
        Assertions.assertThat(createdCaffeDto.getName()).isEqualTo(caffeDto.getName());

        // Caffe caffe = caffeRepository.getByUuid(createdCaffeDto.getUuid());
    }
    @Test
    void shouldUpdateCaffeShop() throws Exception {

        // create an price
        CaffeDto caffeDto = createCaffe();

        final MvcResult mvcResult = mockMvc.perform(post(CAFFE_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(caffeDto))
        ).andExpect(status().isOk()).andReturn();

        final CaffeDto createdCaffeDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), CaffeDto.class);

        Assertions.assertThat(createdCaffeDto.getActive()).isEqualTo(caffeDto.getActive());
        Assertions.assertThat(createdCaffeDto.getAddress()).isEqualTo(caffeDto.getAddress());
        Assertions.assertThat(createdCaffeDto.getContactEmail()).isEqualTo(caffeDto.getContactEmail());
        Assertions.assertThat(createdCaffeDto.getContactPhone()).isEqualTo(caffeDto.getContactPhone());
        Assertions.assertThat(createdCaffeDto.getContactName()).isEqualTo(caffeDto.getContactName());
        Assertions.assertThat(createdCaffeDto.getName()).isEqualTo(caffeDto.getName());
        Caffe caffe = caffeRepository.getByUuid(createdCaffeDto.getUuid());


        caffeDto.setName( "caffeshop-update" );
        caffeDto.setUuid( "uuid-local-update-caffe" );
        caffeDto.setActive( true );
        caffeDto.setName( "caffeShop" );
        caffeDto.setAddress( "local address-update" );
        caffeDto.setContactEmail("@caffemail");
        caffeDto.setContactName("reference-by-update");
        caffeDto.setContactPhone( "0987654321" );

        final MvcResult mvcResultUpdate = mockMvc.perform(put(CAFFE_URL_UPDATE, createdCaffeDto.getUuid())
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(caffeDto))
        ).andExpect(status().isOk()).andReturn();

        final CaffeDto updateCaffeDto = TestUtil.convertBytesToObject(mvcResultUpdate.getResponse().getContentAsByteArray(), CaffeDto.class);

        Assertions.assertThat(updateCaffeDto.getActive()).isEqualTo(caffeDto.getActive());
        Assertions.assertThat(updateCaffeDto.getAddress()).isEqualTo(caffeDto.getAddress());
        Assertions.assertThat(updateCaffeDto.getContactEmail()).isEqualTo(caffeDto.getContactEmail());
        Assertions.assertThat(updateCaffeDto.getContactPhone()).isEqualTo(caffeDto.getContactPhone());
        Assertions.assertThat(updateCaffeDto.getContactName()).isEqualTo(caffeDto.getContactName());
        Assertions.assertThat(updateCaffeDto.getName()).isEqualTo(caffeDto.getName());
    }
    @Test
    void shouldDeleteCaffeShop() throws Exception {

        // create an price
        CaffeDto caffeDto = createCaffe();

        final MvcResult mvcResult = mockMvc.perform(post(CAFFE_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(caffeDto))
        ).andExpect(status().isOk()).andReturn();

        final CaffeDto createdCaffeDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), CaffeDto.class);

        Assertions.assertThat(createdCaffeDto.getActive()).isEqualTo(caffeDto.getActive());
        Assertions.assertThat(createdCaffeDto.getAddress()).isEqualTo(caffeDto.getAddress());
        Assertions.assertThat(createdCaffeDto.getContactEmail()).isEqualTo(caffeDto.getContactEmail());
        Assertions.assertThat(createdCaffeDto.getContactPhone()).isEqualTo(caffeDto.getContactPhone());
        Assertions.assertThat(createdCaffeDto.getContactName()).isEqualTo(caffeDto.getContactName());
        Assertions.assertThat(createdCaffeDto.getName()).isEqualTo(caffeDto.getName());
        Caffe caffe = caffeRepository.getByUuid(createdCaffeDto.getUuid());

        mockMvc.perform(delete(CAFFE_URL_UPDATE, caffe.getUuid())
                .contentType( MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();

        Assertions.assertThat( caffeRepository.getByUuid( createdCaffeDto.getUuid() )).isNull();
    }
    @Test
    void shouldGetCaffeShop() throws Exception {

        // create an price
        CaffeDto caffeDto = createCaffe();

        final MvcResult mvcResult = mockMvc.perform(post(CAFFE_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(caffeDto))
        ).andExpect(status().isOk()).andReturn();

        final CaffeDto createdCaffeDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), CaffeDto.class);

        Assertions.assertThat(createdCaffeDto.getActive()).isEqualTo(caffeDto.getActive());
        Assertions.assertThat(createdCaffeDto.getAddress()).isEqualTo(caffeDto.getAddress());
        Assertions.assertThat(createdCaffeDto.getContactEmail()).isEqualTo(caffeDto.getContactEmail());
        Assertions.assertThat(createdCaffeDto.getContactPhone()).isEqualTo(caffeDto.getContactPhone());
        Assertions.assertThat(createdCaffeDto.getContactName()).isEqualTo(caffeDto.getContactName());
        Assertions.assertThat(createdCaffeDto.getName()).isEqualTo(caffeDto.getName());
        Caffe caffe = caffeRepository.getByUuid(createdCaffeDto.getUuid());

        final MvcResult mvcResultGet = mockMvc.perform(get(CAFFE_URL_UPDATE, caffe.getUuid())
                .contentType( MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();
        final CaffeDto getCaffeDto = TestUtil.convertBytesToObject(mvcResultGet.getResponse().getContentAsByteArray(), CaffeDto.class);
        Assertions.assertThat(createdCaffeDto.getActive()).isEqualTo(getCaffeDto.getActive());
        Assertions.assertThat(createdCaffeDto.getAddress()).isEqualTo(getCaffeDto.getAddress());
        Assertions.assertThat(createdCaffeDto.getContactEmail()).isEqualTo(getCaffeDto.getContactEmail());
        Assertions.assertThat(createdCaffeDto.getContactPhone()).isEqualTo(getCaffeDto.getContactPhone());
        Assertions.assertThat(createdCaffeDto.getContactName()).isEqualTo(getCaffeDto.getContactName());
        Assertions.assertThat(createdCaffeDto.getName()).isEqualTo(getCaffeDto.getName());
    }

    private CaffeDto createCaffe() {
        CaffeDto caffeDto = new CaffeDto();
        caffeDto.setUuid( "uuid-local-test-caffe" );
        caffeDto.setActive( true );
        caffeDto.setName( "caffeShop" );
        caffeDto.setAddress( "local address" );
        caffeDto.setContactEmail("@caffemail");
        caffeDto.setContactName("reference-by");
        caffeDto.setContactPhone( "0987654321" );
        return caffeDto;
    }
}