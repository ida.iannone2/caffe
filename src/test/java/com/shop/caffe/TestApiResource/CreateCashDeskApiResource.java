package com.shop.caffe.TestApiResource;

import com.shop.cafe.domain.CashDesk;
import com.shop.cafe.domain.enumeration.CashDeskState;
import com.shop.cafe.repository.CashDeskRepository;
import com.shop.caffe.util.BaseDBTest;
import com.shop.caffe.util.TestUtil;
import com.shop.cafe.web.data.CashDeskDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
public class CreateCashDeskApiResource extends BaseDBTest {

    private static final String CASHDESK_URL = "/api/cashdesks";
    private static final String CASHDESK_URL_UUID = "/api/cashdesks/{uuid}";
    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected CashDeskRepository cashDeskRepository;

    @Test
    void shouldCreateCashDeskShop() throws Exception {

        // create an price
        CashDeskDto cashDeskDto = createCashDesk();

        final MvcResult mvcResult = mockMvc.perform(post(CASHDESK_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(cashDeskDto))
        ).andExpect(status().isOk()).andReturn();

        final CashDeskDto createdCashDeskDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), CashDeskDto.class);

        Assertions.assertThat(createdCashDeskDto.getTotal()).isEqualTo(cashDeskDto.getTotal());
        Assertions.assertThat(createdCashDeskDto.getState()).isEqualTo(cashDeskDto.getState());
        Assertions.assertThat(createdCashDeskDto.getName()).isEqualTo(cashDeskDto.getName());

        // CashDesk cashDesk = cashDeskRepository.getByUuid(createdCashDeskDto.getUuid());
    }
    @Test
    void shouldUpdateCashDeskShop() throws Exception {

        // create an price
        CashDeskDto cashDeskDto = createCashDesk();

        final MvcResult mvcResult = mockMvc.perform(post(CASHDESK_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(cashDeskDto))
        ).andExpect(status().isOk()).andReturn();

        final CashDeskDto createdCashDeskDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), CashDeskDto.class);

        Assertions.assertThat(createdCashDeskDto.getName()).isEqualTo(cashDeskDto.getName());
        Assertions.assertThat(createdCashDeskDto.getTotal()).isEqualTo(cashDeskDto.getTotal());
        Assertions.assertThat(createdCashDeskDto.getState()).isEqualTo(cashDeskDto.getState());
        CashDesk cashDesk = cashDeskRepository.getByUuid(createdCashDeskDto.getUuid());


        cashDeskDto.setName( "cashDeskshop-update" );
        cashDeskDto.setTotal( (float) 12.7 );

        final MvcResult mvcResultUpdate = mockMvc.perform(put(CASHDESK_URL_UUID, createdCashDeskDto.getUuid())
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(cashDeskDto))
        ).andExpect(status().isOk()).andReturn();

        final CashDeskDto updateCashDeskDto = TestUtil.convertBytesToObject(mvcResultUpdate.getResponse().getContentAsByteArray(), CashDeskDto.class);

        Assertions.assertThat(updateCashDeskDto.getName()).isEqualTo(cashDeskDto.getName());
        Assertions.assertThat(updateCashDeskDto.getName()).isEqualTo(cashDeskDto.getName());
        Assertions.assertThat(updateCashDeskDto.getTotal()).isEqualTo(cashDeskDto.getTotal());
        Assertions.assertThat(updateCashDeskDto.getState()).isEqualTo(cashDeskDto.getState());
    }
    @Test
    void shouldDeleteCashDeskShop() throws Exception {

        // create an price
        CashDeskDto cashDeskDto = createCashDesk();

        final MvcResult mvcResult = mockMvc.perform(post(CASHDESK_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(cashDeskDto))
        ).andExpect(status().isOk()).andReturn();

        final CashDeskDto createdCashDeskDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), CashDeskDto.class);

        Assertions.assertThat(createdCashDeskDto.getName()).isEqualTo(cashDeskDto.getName());
        Assertions.assertThat(createdCashDeskDto.getTotal()).isEqualTo(cashDeskDto.getTotal());
        Assertions.assertThat(createdCashDeskDto.getState()).isEqualTo(cashDeskDto.getState());
        CashDesk cashDesk = cashDeskRepository.getByUuid(createdCashDeskDto.getUuid());

        mockMvc.perform(delete(CASHDESK_URL_UUID, cashDesk.getUuid())
                .contentType( MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();

        Assertions.assertThat( cashDeskRepository.getByUuid( createdCashDeskDto.getUuid() )).isNull();
    }
    @Test
    void shouldGetCashDeskShop() throws Exception {

        // create an price
        CashDeskDto cashDeskDto = createCashDesk();

        final MvcResult mvcResult = mockMvc.perform(post(CASHDESK_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(cashDeskDto))
        ).andExpect(status().isOk()).andReturn();

        final CashDeskDto createdCashDeskDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), CashDeskDto.class);

        Assertions.assertThat(createdCashDeskDto.getName()).isEqualTo(cashDeskDto.getName());
        Assertions.assertThat(createdCashDeskDto.getTotal()).isEqualTo(cashDeskDto.getTotal());
        Assertions.assertThat(createdCashDeskDto.getState()).isEqualTo(cashDeskDto.getState());
        CashDesk cashDesk = cashDeskRepository.getByUuid(createdCashDeskDto.getUuid());

        final MvcResult mvcResultGet = mockMvc.perform(get(CASHDESK_URL_UUID, cashDesk.getUuid())
                .contentType( MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();
        final CashDeskDto getCashDeskDto = TestUtil.convertBytesToObject(mvcResultGet.getResponse().getContentAsByteArray(), CashDeskDto.class);

        Assertions.assertThat(createdCashDeskDto.getName()).isEqualTo(getCashDeskDto.getName());
        Assertions.assertThat(createdCashDeskDto.getName()).isEqualTo(getCashDeskDto.getName());
        Assertions.assertThat(createdCashDeskDto.getTotal()).isEqualTo(getCashDeskDto.getTotal());
        Assertions.assertThat(createdCashDeskDto.getState()).isEqualTo(getCashDeskDto.getState());
    }

    private CashDeskDto createCashDesk() {
        CashDeskDto cashDeskDto = new CashDeskDto();
        cashDeskDto.setUuid( "uuid-local-test-cashDesk" );
        cashDeskDto.setState( CashDeskState.OPEN );
        cashDeskDto.setName( "cashDeskShop" );
        cashDeskDto.setTotal( (float) 23.4 );

        return cashDeskDto;
    }
}
