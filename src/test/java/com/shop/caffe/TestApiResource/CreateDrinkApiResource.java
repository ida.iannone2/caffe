package com.shop.caffe.TestApiResource;

import com.shop.cafe.domain.Drink;
import com.shop.cafe.repository.DrinkRepository;
import com.shop.caffe.util.BaseDBTest;
import com.shop.caffe.util.TestUtil;
import com.shop.cafe.web.data.DrinkDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
public class CreateDrinkApiResource extends BaseDBTest {

    private static final String FOOD_URL = "/api/drinks";
    private static final String FOOD_URL_UUID = "/api/drinks/{uuid}";
    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected DrinkRepository drinkRepository;

    @Test
    void shouldCreateDrinkShop() throws Exception {

        // create an price
        DrinkDto drinkDto = createDrink();

        final MvcResult mvcResult = mockMvc.perform(post(FOOD_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(drinkDto))
        ).andExpect(status().isOk()).andReturn();

        final DrinkDto createdDrinkDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), DrinkDto.class);

        Assertions.assertThat(createdDrinkDto.getPrice()).isEqualTo(drinkDto.getPrice());
        Assertions.assertThat(createdDrinkDto.getType()).isEqualTo(drinkDto.getType());
        Assertions.assertThat(createdDrinkDto.getName()).isEqualTo(drinkDto.getName());

        // Drink drink = drinkRepository.getByUuid(createdDrinkDto.getUuid());
    }
    @Test
    void shouldUpdateDrinkShop() throws Exception {

        // create an price
        DrinkDto drinkDto = createDrink();

        final MvcResult mvcResult = mockMvc.perform(post(FOOD_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(drinkDto))
        ).andExpect(status().isOk()).andReturn();

        final DrinkDto createdDrinkDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), DrinkDto.class);

        Assertions.assertThat(createdDrinkDto.getName()).isEqualTo(drinkDto.getName());
        Assertions.assertThat(createdDrinkDto.getPrice()).isEqualTo(drinkDto.getPrice());
        Assertions.assertThat(createdDrinkDto.getType()).isEqualTo(drinkDto.getType());
        Drink drink = drinkRepository.getByUuid(createdDrinkDto.getUuid());


        drinkDto.setName( "drinkshop-update" );
        drinkDto.setPrice( 12.7 );

        final MvcResult mvcResultUpdate = mockMvc.perform(put(FOOD_URL_UUID, createdDrinkDto.getUuid())
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(drinkDto))
        ).andExpect(status().isOk()).andReturn();

        final DrinkDto updateDrinkDto = TestUtil.convertBytesToObject(mvcResultUpdate.getResponse().getContentAsByteArray(), DrinkDto.class);

        Assertions.assertThat(updateDrinkDto.getName()).isEqualTo(drinkDto.getName());
        Assertions.assertThat(updateDrinkDto.getName()).isEqualTo(drinkDto.getName());
        Assertions.assertThat(updateDrinkDto.getPrice()).isEqualTo(drinkDto.getPrice());
        Assertions.assertThat(updateDrinkDto.getType()).isEqualTo(drinkDto.getType());
    }
    @Test
    void shouldDeleteDrinkShop() throws Exception {

        // create an price
        DrinkDto drinkDto = createDrink();

        final MvcResult mvcResult = mockMvc.perform(post(FOOD_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(drinkDto))
        ).andExpect(status().isOk()).andReturn();

        final DrinkDto createdDrinkDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), DrinkDto.class);

        Assertions.assertThat(createdDrinkDto.getName()).isEqualTo(drinkDto.getName());
        Assertions.assertThat(createdDrinkDto.getPrice()).isEqualTo(drinkDto.getPrice());
        Assertions.assertThat(createdDrinkDto.getType()).isEqualTo(drinkDto.getType());
        Drink drink = drinkRepository.getByUuid(createdDrinkDto.getUuid());

        mockMvc.perform(delete(FOOD_URL_UUID, drink.getUuid())
                .contentType( MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();

        Assertions.assertThat( drinkRepository.getByUuid( createdDrinkDto.getUuid() )).isNull();
    }
    @Test
    void shouldGetDrinkShop() throws Exception {

        // create an price
        DrinkDto drinkDto = createDrink();

        final MvcResult mvcResult = mockMvc.perform(post(FOOD_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(drinkDto))
        ).andExpect(status().isOk()).andReturn();

        final DrinkDto createdDrinkDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), DrinkDto.class);

        Assertions.assertThat(createdDrinkDto.getName()).isEqualTo(drinkDto.getName());
        Assertions.assertThat(createdDrinkDto.getPrice()).isEqualTo(drinkDto.getPrice());
        Assertions.assertThat(createdDrinkDto.getType()).isEqualTo(drinkDto.getType());
        Drink drink = drinkRepository.getByUuid(createdDrinkDto.getUuid());

        final MvcResult mvcResultGet = mockMvc.perform(get(FOOD_URL_UUID, drink.getUuid())
                .contentType( MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();
        final DrinkDto getDrinkDto = TestUtil.convertBytesToObject(mvcResultGet.getResponse().getContentAsByteArray(), DrinkDto.class);

        Assertions.assertThat(createdDrinkDto.getName()).isEqualTo(getDrinkDto.getName());
        Assertions.assertThat(createdDrinkDto.getName()).isEqualTo(getDrinkDto.getName());
        Assertions.assertThat(createdDrinkDto.getPrice()).isEqualTo(getDrinkDto.getPrice());
        Assertions.assertThat(createdDrinkDto.getType()).isEqualTo(getDrinkDto.getType());
    }

    private DrinkDto createDrink() {
        DrinkDto drinkDto = new DrinkDto();
        drinkDto.setUuid( "uuid-local-test-drink" );
        drinkDto.setType("cocktail");
        drinkDto.setName( "drinkShop" );
        drinkDto.setPrice( 5.4 );

        return drinkDto;
    }
}