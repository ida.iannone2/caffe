package com.shop.caffe.TestApiResource;

import com.shop.cafe.domain.Cart;
import com.shop.cafe.domain.Drink;
import com.shop.cafe.domain.enumeration.CartItemType;
import com.shop.cafe.domain.enumeration.CartState;
import com.shop.cafe.repository.CartRepository;
import com.shop.cafe.repository.DrinkRepository;
import com.shop.cafe.service.mapper.CartMapper;
import com.shop.caffe.util.BaseDBTest;
import com.shop.caffe.util.TestUtil;
import com.shop.cafe.web.data.CartDto;
import com.shop.cafe.web.data.CartItemDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CreateCartAndCartIItemApiResource extends BaseDBTest {

    private static final String FOOD_URL = "/api/drinks";
    private static final String FOOD_URL_UUID = "/api/drinks/{uuid}";

    private static final String CART_URL = "/api/carts";
    private static final String CART_URL_UUID = "/api/carts/{uuid}";
    private static final String CART_URL_UUID_ITEM = "/api/carts/{uuid}/items";
    private static final String CART_URL_UUID_ITEM_UUID = "/api/carts/{uuid}/items/{uuid}";
    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected CartRepository cartRepository;
    @Autowired
    protected DrinkRepository drinkRepository;
    @Autowired
    protected CartMapper cartMapper;
    @Test
    @BeforeEach
    void shouldCreateCartCorrectly() throws Exception {

        final CartDto cartDto = createCartDto();

        final MvcResult mvcResult = mockMvc.perform(post(CART_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(cartDto)))
                .andExpect(status().isOk())
                .andReturn();

        CartDto cart = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), CartDto.class);
        Assertions.assertThat(cart.getUuid()).isNotNull();

        final String cartUuid = cart.getUuid();
        final Cart cart1 = cartRepository.findByUuid(cartUuid).get();


        Assertions.assertThat(cart1.getUuid()).isEqualTo(cartUuid);
        Assertions.assertThat(cart1.getConsumerReference()).isEqualTo( cart.getConsumerReference() );
        Assertions.assertThat(cart1.getState()).isEqualTo(cart.getState());
        Assertions.assertThat(cart1.getTotalPrice().floatValue()).isEqualTo(cart.getTotalPrice());
    }
    @Test
    void shouldUpdateCartShop() throws Exception {
        // create an price
        CartDto cartDto = createCartDto();

        final MvcResult mvcResult = mockMvc.perform(post(CART_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(cartDto))
        ).andExpect(status().isOk()).andReturn();

        final CartDto createdCartDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), CartDto.class);

        Assertions.assertThat(createdCartDto.getConsumerReference()).isEqualTo(cartDto.getConsumerReference());
        Assertions.assertThat(createdCartDto.getCovered()).isEqualTo(cartDto.getCovered());
        Assertions.assertThat(createdCartDto.getCurrency()).isEqualTo(cartDto.getCurrency());
        Cart cart = cartRepository.getByUuid(createdCartDto.getUuid());

        cartDto.setCovered(2.9f);
        cartDto.setCurrency("usd");

        final MvcResult mvcResultUpdate = mockMvc.perform(put(CART_URL_UUID, cart.getUuid())
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(cartDto))
        ).andExpect(status().isOk()).andReturn();

        final CartDto updateCartDto = TestUtil.convertBytesToObject(mvcResultUpdate.getResponse().getContentAsByteArray(), CartDto.class);

        Assertions.assertThat(updateCartDto.getConsumerReference()).isEqualTo(cartDto.getConsumerReference());
        Assertions.assertThat(updateCartDto.getCovered()).isEqualTo(cartDto.getCovered());
        Assertions.assertThat(updateCartDto.getCurrency()).isEqualTo(cartDto.getCurrency());
    }
    @Test
    void shouldDeleteCartShop() throws Exception {

        // create an price
        CartDto cartDto = createCartDto();

        final MvcResult mvcResult = mockMvc.perform(post(CART_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(cartDto))
        ).andExpect(status().isOk()).andReturn();

        final CartDto createdCartDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), CartDto.class);

        Assertions.assertThat(createdCartDto.getConsumerReference()).isEqualTo(cartDto.getConsumerReference());
        Assertions.assertThat(createdCartDto.getCovered()).isEqualTo(cartDto.getCovered());
        Assertions.assertThat(createdCartDto.getCurrency()).isEqualTo(cartDto.getCurrency());
        Cart cart = cartRepository.getByUuid(createdCartDto.getUuid());

        mockMvc.perform(delete(CART_URL_UUID, cart.getUuid())
                .contentType( MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();

        Assertions.assertThat( cartRepository.getByUuid( createdCartDto.getUuid() )).isNull();
    }
    @Test
    void shouldGetCartShop() throws Exception {

        // create an price
        CartDto cartDto = createCartDto();

        final MvcResult mvcResult = mockMvc.perform(post(CART_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(cartDto))
        ).andExpect(status().isOk()).andReturn();

        final CartDto createdCartDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), CartDto.class);

        Cart cart = cartRepository.getByUuid(createdCartDto.getUuid());

        final MvcResult mvcResultGet = mockMvc.perform(get(CART_URL_UUID, cart.getUuid())
                .contentType( MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();
        final CartDto getCartDto = TestUtil.convertBytesToObject(mvcResultGet.getResponse().getContentAsByteArray(), CartDto.class);

        Assertions.assertThat(getCartDto.getConsumerReference()).isEqualTo(cartDto.getConsumerReference());
        Assertions.assertThat(getCartDto.getCovered()).isEqualTo(cartDto.getCovered());
        Assertions.assertThat(getCartDto.getCurrency()).isEqualTo(cartDto.getCurrency());

    }


    private CartDto createCartDto(){
        CartDto cartDto = new CartDto();
        cartDto.setUuid( "cart-uuid" );
        cartDto.setState(CartState.CREATE);
        cartDto.setItems( Set.of());
        cartDto.setCurrency( "eur" );
        cartDto.setNotes("notes");
        cartDto.setConsumerReference( "user" );
        cartDto.setCovered(1.5f);
        cartDto.setTotalPrice( 3.0f );
        return cartDto;
    }
    private CartItemDto createCartItem(String cartuuid, String element){
        CartItemDto cartItemDto = new CartItemDto();
        cartItemDto.setCartUuid(cartuuid);
        cartItemDto.setPrice(3.0f);
        cartItemDto.setQuantity(2);
        cartItemDto.setType( CartItemType.DRINK);
        cartItemDto.setUuid(element);
        return cartItemDto;
    }
    private Drink createDrink() {
        Drink drinkDto = new Drink();
        drinkDto.setUuid( "uuid-local-test-drink" );
        drinkDto.setType("cocktail");
        drinkDto.setName( "drinkShop" );
        drinkDto.setPrice( 5.4 );

        return drinkDto;
    }
    @Test
    void shoudAddItemCorrectly() throws Exception {
        Drink drinktmp = createDrink();
        Drink drink = drinkRepository.save( drinktmp );
        final CartDto cartDto = createCartDto();

        final MvcResult mvcResult = mockMvc.perform( post( CART_URL )
                .contentType( MediaType.APPLICATION_JSON )
                .content( TestUtil.convertObjectToJsonBytes( cartDto ) ) )
                .andExpect( status().isOk() )
                .andReturn();

        CartDto cart = TestUtil.convertBytesToObject( mvcResult.getResponse().getContentAsByteArray(), CartDto.class );
        CartItemDto cartItemDto = createCartItem( cart.getUuid(), cart.getUuid() );

        final MvcResult mvcResultItem = mockMvc.perform( post( CART_URL_UUID_ITEM, cart.getUuid() )
                .contentType( MediaType.APPLICATION_JSON )
                .content( TestUtil.convertObjectToJsonBytes( cartItemDto ) )
        ).andExpect( status().isOk() ).andReturn();
        final CartDto getCartDto = TestUtil.convertBytesToObject( mvcResultItem.getResponse().getContentAsByteArray(), CartDto.class );

        Assertions.assertThat( getCartDto.getItems().stream().filter( cartItem -> cartItem.getUuid().equals( drink.getUuid() ) ) );
        // shoudGetItemCorrectly(){

    }

/**   final MvcResult mvcResultItemGet = mockMvc.perform(get(CART_URL_UUID_ITEM_UUID, getCartDto.getUuid(), drink.getUuid())
 .contentType( MediaType.APPLICATION_JSON)
 .content( TestUtil.convertObjectToJsonBytes(cartItemDto))
 ).andExpect(status().isOk()).andReturn();
 final CartDto getCartItemDto2 = TestUtil.convertBytesToObject(mvcResultItemGet.getResponse().getContentAsByteArray(), CartDto.class);
 Assertions.assertThat( getCartItemDto2.getItems().stream().filter(cartItem -> cartItem.getUuid().equals( drink.getUuid()))).isEqualTo( true );

 //void shoudUpdateItemCorrectly(){
 cartItemDto.setPrice( 5f );
 final MvcResult mvcResultItemUp = mockMvc.perform(put(CART_URL_UUID_ITEM_UUID, cart.getUuid(),drink.getUuid())
 .contentType( MediaType.APPLICATION_JSON)
 .content( TestUtil.convertObjectToJsonBytes(cartItemDto))
 ).andExpect(status().isOk()).andReturn();
 final CartDto getCartItemDto3 = TestUtil.convertBytesToObject(mvcResultItemUp.getResponse().getContentAsByteArray(), CartDto.class);
 Assertions.assertThat( getCartItemDto3.getItems().stream().filter(cartItem -> cartItem.getUuid().equals( drink.getUuid())));

 }
 void shoudDeleteItemCorrectly(){}/**/
 }