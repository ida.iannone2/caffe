package com.shop.caffe.TestApiResource;

import com.shop.cafe.domain.Food;
import com.shop.cafe.repository.FoodRepository;
import com.shop.caffe.util.BaseDBTest;
import com.shop.caffe.util.TestUtil;
import com.shop.cafe.web.data.FoodDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
public class CreateFoodApiResource extends BaseDBTest {

    private static final String FOOD_URL = "/api/foods";
    private static final String FOOD_URL_UUID = "/api/foods/{uuid}";
    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected FoodRepository foodRepository;

    @Test
    void shouldCreateFoodShop() throws Exception {

        // create an price
        FoodDto foodDto = createFood();

        final MvcResult mvcResult = mockMvc.perform(post(FOOD_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(foodDto))
        ).andExpect(status().isOk()).andReturn();

        final FoodDto createdFoodDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), FoodDto.class);

        Assertions.assertThat(createdFoodDto.getPrice()).isEqualTo(foodDto.getPrice());
        Assertions.assertThat(createdFoodDto.getType()).isEqualTo(foodDto.getType());
        Assertions.assertThat(createdFoodDto.getName()).isEqualTo(foodDto.getName());

        // Food food = foodRepository.getByUuid(createdFoodDto.getUuid());
    }
    @Test
    void shouldUpdateFoodShop() throws Exception {

        // create an price
        FoodDto foodDto = createFood();

        final MvcResult mvcResult = mockMvc.perform(post(FOOD_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(foodDto))
        ).andExpect(status().isOk()).andReturn();

        final FoodDto createdFoodDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), FoodDto.class);

        Assertions.assertThat(createdFoodDto.getName()).isEqualTo(foodDto.getName());
        Assertions.assertThat(createdFoodDto.getPrice()).isEqualTo(foodDto.getPrice());
        Assertions.assertThat(createdFoodDto.getType()).isEqualTo(foodDto.getType());
        Food food = foodRepository.getByUuid(createdFoodDto.getUuid());


        foodDto.setName( "foodshop-update" );
        foodDto.setPrice( 12.7 );

        final MvcResult mvcResultUpdate = mockMvc.perform(put(FOOD_URL_UUID, createdFoodDto.getUuid())
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(foodDto))
        ).andExpect(status().isOk()).andReturn();

        final FoodDto updateFoodDto = TestUtil.convertBytesToObject(mvcResultUpdate.getResponse().getContentAsByteArray(), FoodDto.class);

        Assertions.assertThat(updateFoodDto.getName()).isEqualTo(foodDto.getName());
        Assertions.assertThat(updateFoodDto.getName()).isEqualTo(foodDto.getName());
        Assertions.assertThat(updateFoodDto.getPrice()).isEqualTo(foodDto.getPrice());
        Assertions.assertThat(updateFoodDto.getType()).isEqualTo(foodDto.getType());
    }
    @Test
    void shouldDeleteFoodShop() throws Exception {

        // create an price
        FoodDto foodDto = createFood();

        final MvcResult mvcResult = mockMvc.perform(post(FOOD_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(foodDto))
        ).andExpect(status().isOk()).andReturn();

        final FoodDto createdFoodDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), FoodDto.class);

        Assertions.assertThat(createdFoodDto.getName()).isEqualTo(foodDto.getName());
        Assertions.assertThat(createdFoodDto.getPrice()).isEqualTo(foodDto.getPrice());
        Assertions.assertThat(createdFoodDto.getType()).isEqualTo(foodDto.getType());
        Food food = foodRepository.getByUuid(createdFoodDto.getUuid());

        mockMvc.perform(delete(FOOD_URL_UUID, food.getUuid())
                .contentType( MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();

        Assertions.assertThat( foodRepository.getByUuid( createdFoodDto.getUuid() )).isNull();
    }
    @Test
    void shouldGetFoodShop() throws Exception {

        // create an price
        FoodDto foodDto = createFood();

        final MvcResult mvcResult = mockMvc.perform(post(FOOD_URL)
                .contentType( MediaType.APPLICATION_JSON)
                .content( TestUtil.convertObjectToJsonBytes(foodDto))
        ).andExpect(status().isOk()).andReturn();

        final FoodDto createdFoodDto = TestUtil.convertBytesToObject(mvcResult.getResponse().getContentAsByteArray(), FoodDto.class);

        Assertions.assertThat(createdFoodDto.getName()).isEqualTo(foodDto.getName());
        Assertions.assertThat(createdFoodDto.getPrice()).isEqualTo(foodDto.getPrice());
        Assertions.assertThat(createdFoodDto.getType()).isEqualTo(foodDto.getType());
        Food food = foodRepository.getByUuid(createdFoodDto.getUuid());

        final MvcResult mvcResultGet = mockMvc.perform(get(FOOD_URL_UUID, food.getUuid())
                .contentType( MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();
        final FoodDto getFoodDto = TestUtil.convertBytesToObject(mvcResultGet.getResponse().getContentAsByteArray(), FoodDto.class);

        Assertions.assertThat(createdFoodDto.getName()).isEqualTo(getFoodDto.getName());
        Assertions.assertThat(createdFoodDto.getName()).isEqualTo(getFoodDto.getName());
        Assertions.assertThat(createdFoodDto.getPrice()).isEqualTo(getFoodDto.getPrice());
        Assertions.assertThat(createdFoodDto.getType()).isEqualTo(getFoodDto.getType());
    }

    private FoodDto createFood() {
        FoodDto foodDto = new FoodDto();
        foodDto.setUuid( "uuid-local-test-food" );
        foodDto.setType("PANINO");
        foodDto.setName( "foodShop" );
        foodDto.setPrice( 23.4 );

        return foodDto;
    }
}
