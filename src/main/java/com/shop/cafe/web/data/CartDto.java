package com.shop.cafe.web.data;

import com.shop.cafe.domain.CartItem;
import com.shop.cafe.domain.enumeration.CartState;
import lombok.Data;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
public class CartDto implements Serializable {

    private String uuid;
    private String currency;

    private String consumerReference;
    private String notes;

    private CartState state;

    private Set<CartItem> items = new HashSet<>();

    private Float totalPrice;
    private Float covered;
}

