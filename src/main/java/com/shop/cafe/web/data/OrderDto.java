package com.shop.cafe.web.data;

import com.shop.cafe.domain.enumeration.OrderState;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class OrderDto implements Serializable {

    private String uuid;

    //delivery or local
    private String type;
    //in progress, done, topay, closed, paid
    private LocalDate date;

    //in progress, done, topay, closed, paid    private OrderState state;
    private OrderState state;

    private BigDecimal totalPrice;
    private String cartuuid;
}
