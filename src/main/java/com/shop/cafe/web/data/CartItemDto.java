package com.shop.cafe.web.data;

import com.shop.cafe.domain.enumeration.CartItemType;
import lombok.Data;

import java.io.Serializable;
@Data
public class CartItemDto implements Serializable {
    //uuid element in cart
    private String uuid;
    //Food or drink
    private CartItemType type;
    private Float price;
    private Integer quantity;
    private String notes;
    private String cartUuid;
}
