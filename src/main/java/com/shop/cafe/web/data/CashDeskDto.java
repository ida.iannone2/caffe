package com.shop.cafe.web.data;

import com.shop.cafe.domain.enumeration.CashDeskState;
import lombok.Data;

import java.io.Serializable;

@Data
public class CashDeskDto implements Serializable {
    private Long id;
    private String name;
    private String uuid;
    private Float total;
    private CashDeskState state;
}
