package com.shop.cafe.web.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class DrinkDto implements Serializable {
    private String uuid;

    private String name;
    private String type;
    private Double price;
}
