package com.shop.cafe.web.data;

import lombok.Data;
import java.io.Serializable;


@Data
public class CaffeDto implements Serializable {

    private String uuid;
    private String name;
    private Boolean active;
    private String address;
    private String contactName;
    private String contactPhone;
    private String contactEmail;
}
