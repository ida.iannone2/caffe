package com.shop.cafe.domain;

import com.shop.cafe.domain.enumeration.CartState;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "cart")
public class Cart implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "uuid")
    private String uuid;
    @Column(name = "currency")
    private String currency;
    @Column(name = "consumerReference")
    private String consumerReference;

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private CartState state;

    @OneToMany(mappedBy = "cart")
    private Set<CartItem> items = new HashSet<>();
    @Column(name = "total_price")
    private BigDecimal totalPrice;
    @Column(name = "covered")
    private BigDecimal covered;

    public Cart addItem(CartItem cartItem) {
        this.items.add(cartItem);
        cartItem.setCart(this);
        return this;
    }

    public Cart removeItem(CartItem cartItem) {
        this.items.remove(cartItem);
        cartItem.setCart(null);
        return this;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cart)) {
            return false;
        }
        return id != null && id.equals(((Cart) o).id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }

    @Override
    public String toString() {
        return "Cart{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", currency='" + currency + '\'' +
                ", consumerReference='" + consumerReference + '\'' +
                ", state=" + state +
                ", items=" + items +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
