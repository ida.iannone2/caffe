package com.shop.cafe.domain;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "drink")
@Getter
@Setter
public class Drink {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "uuid", nullable = false)
    private String uuid;
    @Column(name = "name")
    private String name;
    @Column(name = "type")
    private String type;
    @Column(name = "price")
    private Double price;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Drink)) return false;
        Drink drink = (Drink) o;
        return Objects.equals( getId(), drink.getId() ) &&
                Objects.equals( getUuid(), drink.getUuid() ) &&
                Objects.equals( getName(), drink.getName() ) &&
                Objects.equals( getType(), drink.getType() ) &&
                Objects.equals( getPrice(), drink.getPrice() );
    }

    @Override
    public int hashCode() {
        return Objects.hash( getId(), getUuid(), getName(), getType(), getPrice() );
    }

    @Override
    public String toString() {
        return "Drink{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", price=" + price +
                '}';
    }
}
