package com.shop.cafe.domain;

import com.shop.cafe.domain.enumeration.OrderState;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Table(name = "topay")
@Entity
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "uuid")
    private String uuid;

    //delivery or local
    @Column(name = "type")
    private String type;
    //delivery, local, free, closed

    @Column(name = "date")
    private LocalDate date = LocalDate.now();

    //in progress, done, topay, closed, paid    private OrderState state
    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private OrderState state;

    @Column(name = "total_price")
    private BigDecimal totalPrice;

//  @Column(name = "cart")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Cart cart;

}

