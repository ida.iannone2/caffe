package com.shop.cafe.domain;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Getter
@Setter
@Table(name = "food")
public class Food {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
     @Column(name = "uuid", nullable = false)
    private String uuid;
    @Column(name = "name")
    private String name;
    @Column(name = "type")
    private String type;
    @Column(name = "price")
    private Double price;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Food)) return false;
        Food food = (Food) o;
        return Objects.equals( getId(), food.getId() ) &&
                Objects.equals( getUuid(), food.getUuid() ) &&
                Objects.equals( getName(), food.getName() ) &&
                Objects.equals( getType(), food.getType() ) &&
                Objects.equals( getPrice(), food.getPrice() );
    }

    @Override
    public int hashCode() {
        return Objects.hash( getId(), getUuid(), getName(), getType(), getPrice() );
    }

    @Override
    public String toString() {
        return "Food{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", price=" + price +
                '}';
    }
}
