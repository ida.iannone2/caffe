package com.shop.cafe.domain;

import com.shop.cafe.domain.enumeration.CashDeskState;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Table(name = "cash_desk")
public class CashDesk implements Serializable {
    //TODO iplement admin user an then cashdesk
    //when an user pay we increment total.
    //in this table we can calculate material gain, outgouing.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "uuid")
    private String uuid;
    @Column(name = "name")
    private String name;
    @Column(name = "total")
    private BigDecimal total;
    @Enumerated(EnumType.STRING)
    @Column(name = "state")
    private CashDeskState state;

}
