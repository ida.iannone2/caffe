package com.shop.cafe.domain.enumeration;

public enum CashDeskState {

    OPEN( "OPEN" ),

    CLOSED( "CLOSED" );

    private String value;

    CashDeskState(String value) {
        this.value = value;

    }
}