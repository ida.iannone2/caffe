package com.shop.cafe.domain.enumeration;

public enum OrderType {

    DELIVERY( "DELIVERY" ),

    LOCAL( "LOCAL" ),

    FREE( "FREE" );

    private String value;

    OrderType(String value) {
        this.value = value;

    }

}
