package com.shop.cafe.domain.enumeration;

/**
 * The CartState enumeration.
 */
public enum CartState {

    CREATE("CREATE"),

    IN_PROGRESS("IN_PROGRESS"),

    DELETED("DELETED"),
    //TODO check validation
    INVALID("INVALID"),

    CONFIRMED("CONFIRMED");

    private String value;

    CartState(String value) {
        this.value = value;
    }

}
