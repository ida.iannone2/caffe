package com.shop.cafe.domain.enumeration;


public enum CartItemType {

    FOOD( "FOOD" ),

    DRINK( "DRINK" ),

    OFFER( "OFFER" );

    private String value;

    CartItemType(String value) {
        this.value = value;

    }

}