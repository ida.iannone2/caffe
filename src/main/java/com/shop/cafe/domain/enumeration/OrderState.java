package com.shop.cafe.domain.enumeration;

public enum OrderState {

    IN_PROGRESS( "IN_PROGRESS" ),

    DONE( "DONE" ),

    TO_PAY( "TO_PAY" ),
    CLOSED( "CLOSED" );

    private String value;

    OrderState(String value) {
        this.value = value;

    }}
