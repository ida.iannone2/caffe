package com.shop.cafe.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.shop.cafe.domain.enumeration.CartItemType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Table(name = "cartitem")
public class CartItem implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "uuid")
    private String uuid;
    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private CartItemType type;
    @Column(name = "price")
    private BigDecimal price;
    @Column(name = "quantity")
    private Integer quantity;
    @Column(name = "notes")
    private String notes;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIgnoreProperties("items")
    private Cart cart;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CartItem)) {
            return false;
        }
        return id != null && id.equals(((CartItem) o).id);
    }

    @Override
    public String toString() {
        return "CartItem{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", type=" + type +
                ", price=" + price +
                ", quantity=" + quantity +
                ", notes='" + notes + '\'' +
                ", cart=" + cart +
                '}';
    }
}