package com.shop.cafe.domain;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "caffe")
@Getter
@Setter
public class Caffe implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "uuid", nullable = false)
    private String uuid;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

   @NotNull
    @Column(name = "active", nullable = false)
    private Boolean active;

   @Column(name = "address")
    private String address;

    @Column(name = "contact_name")
    private String contactName;

    @Column(name = "contact_phone")
    private String contactPhone;

    @Column(name = "contact_email")
    private String contactEmail;


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Caffe)) {
            return false;
        }
        return id != null && id.equals(((Caffe) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Caffe{" +
                "id=" + getId() +
                ", name='" + getName() + "'" +
                ", uuid='" + getUuid() + "'" +
                ", active='" + getActive() + "'" +
                ", address='" + getAddress() + "'" +
                ", contactName='" + getContactName() + "'" +
                ", contactPhone='" + getContactPhone() + "'" +
                ", contactEmail='" + getContactEmail() + "'" +
                "}";
    }
}
