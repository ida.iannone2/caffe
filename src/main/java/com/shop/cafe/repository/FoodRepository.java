package com.shop.cafe.repository;

import com.shop.cafe.domain.Food;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodRepository extends JpaRepository<Food, Long> {
    Food getByUuid(String uuid);
}
