package com.shop.cafe.repository;

import com.shop.cafe.domain.Caffe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaffeRepository extends JpaRepository<Caffe, Long> {

    Caffe getByUuid(String uuid);
    Caffe save(Caffe caffe);
}
