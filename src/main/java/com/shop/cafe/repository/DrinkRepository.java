package com.shop.cafe.repository;

import com.shop.cafe.domain.Drink;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DrinkRepository extends JpaRepository<Drink, Long> {
    Drink getByUuid(String uuid);
}
