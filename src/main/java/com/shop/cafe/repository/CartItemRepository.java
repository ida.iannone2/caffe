package com.shop.cafe.repository;

import com.shop.cafe.domain.CartItem;
import com.shop.cafe.domain.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface CartItemRepository extends JpaRepository<CartItem, Long> {

    Optional<CartItem> findByUuidAndCart(String uuid, Cart cart);

    CartItem getByUuidAndCart(String uuid, Cart cart);

    List<CartItem> getAllByCart(Cart cart);

    CartItem getByUuid(String uuidItem);
}
