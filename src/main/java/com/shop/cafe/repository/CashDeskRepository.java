package com.shop.cafe.repository;

import com.shop.cafe.domain.CashDesk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CashDeskRepository extends JpaRepository<CashDesk, Long> {
    CashDesk getByUuid(String uuid);

    void delete(CashDesk cashDesk);
}
