package com.shop.cafe.repository;

import com.shop.cafe.domain.Cart;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {

    @EntityGraph(attributePaths = {"items"})
    Optional<Cart> findByUuid(String uuid);

    @EntityGraph(attributePaths = {"items"})
    Cart getByUuid(String uuid);
}
