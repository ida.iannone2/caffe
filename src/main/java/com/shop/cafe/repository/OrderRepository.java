package com.shop.cafe.repository;

import com.shop.cafe.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    Order getByUuid(String uuid);
}
