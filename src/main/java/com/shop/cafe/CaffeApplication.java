package com.shop.cafe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@Configuration
@ComponentScan
@EnableJpaRepositories
public class CaffeApplication {

/**	@Autowired
	LiquibaseProperties properties;
	@Autowired
	private DataSource dataSource;
	/**private String changeLog = "classpath:/db/changelog/db.changelog-master.xml";

	public SpringLiquibase liquibase() {
		SpringLiquibase liquibase = new SpringLiquibase();
		properties.setChangeLog(changeLog);
		liquibase.setChangeLog(this.properties.getChangeLog());
		liquibase.setContexts(this.properties.getContexts());
		liquibase.setDataSource(this.dataSource);
		liquibase.setDefaultSchema(this.properties.getDefaultSchema());
		liquibase.setDropFirst(this.properties.isDropFirst());
		liquibase.setShouldRun(this.properties.isEnabled());
		return liquibase;
	}*/
	public static void main(String[] args) {
		SpringApplication.run(CaffeApplication.class, args);

	}
}
