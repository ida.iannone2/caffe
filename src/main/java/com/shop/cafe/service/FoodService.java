package com.shop.cafe.service;

import com.shop.cafe.web.data.FoodDto;
import com.shop.cafe.domain.Food;
import com.shop.cafe.repository.FoodRepository;
import com.shop.cafe.service.mapper.FoodMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class FoodService {

    @Autowired
    FoodRepository foodRepository;
    private final FoodMapper foodMapper;

    public FoodService(FoodMapper foodMapper) {
        this.foodMapper = foodMapper;
    }

    @Transactional
    public FoodDto create(FoodDto foodDto) {
        //Food
        Food food = foodMapper.toFood(foodDto);
        food.setUuid( UUID.randomUUID().toString() );
        //Save Food
        Food savedfood = foodRepository.save(food);

        //Convert to DTO
        return foodMapper.toFoodDto(savedfood);
    }

    public List<FoodDto> getAll(Pageable pageable) {
        return foodRepository.findAll(pageable).stream()
                .map( foodMapper::toFoodDto).collect( Collectors.toList());
    }

    public FoodDto getByUuid(String uuid) {
        final Food food = foodRepository.getByUuid(uuid);
        return foodMapper.toFoodDto( food );
    }

    @Transactional
    public FoodDto update(String uuid, FoodDto foodDto) {
        Food foodUpdated = foodMapper.toFoodUpdate(uuid, foodDto);
        Food savedFood = foodRepository.save( foodUpdated );
        return foodMapper.toFoodDto(savedFood);
    }

    public String delete(String uuid){
        Food food = foodRepository.getByUuid(uuid);
        foodRepository.delete( food );
        return "deleted: "+ food.getUuid();
    }

}
