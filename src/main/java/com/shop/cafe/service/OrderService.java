package com.shop.cafe.service;

import com.shop.cafe.service.component.OrderComponent;
import com.shop.cafe.domain.Cart;
import com.shop.cafe.domain.Order;
import com.shop.cafe.domain.enumeration.CartState;
import com.shop.cafe.repository.OrderRepository;
import com.shop.cafe.service.mapper.OrderMapper;
import com.shop.cafe.web.data.OrderDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderService {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    private final OrderMapper orderMapper;
    @Autowired
    private final OrderComponent orderComponent;

    @Transactional
    public OrderDto create(OrderDto orderDto) {
        //Order
        //TODO we can create a new order if we have a cart to pay
        Order order = orderMapper.toNewOrder(orderDto);
        Cart cart = order.getCart();
        cart.setState(CartState.CONFIRMED);
        orderComponent.calculateTotalPrice(order, cart);
        //Save Order
        Order savedorder = orderRepository.save(order);
        //Convert to DTO
        return orderMapper.toOrderDto(savedorder);
    }

    public List<OrderDto> getAll(Pageable pageable) {
        return orderRepository.findAll(pageable).stream()
                .map( orderMapper::toOrderDto).collect( Collectors.toList());
    }

    public OrderDto getByUuid(String uuid) {
        final Order order = orderRepository.getByUuid(uuid);
        return orderMapper.toOrderDto( order );
    }

    @Transactional
    public OrderDto update(String uuid, OrderDto orderDto) {
        Order orderUpdated = orderMapper.toOrderUpdate(uuid, orderDto);
        Order savedOrder = orderRepository.save( orderUpdated );
        orderComponent.calculateTotalPrice(savedOrder, savedOrder.getCart());
        return orderMapper.toOrderDto(savedOrder);
    }

    public String delete(String uuid){
        Order order = orderRepository.getByUuid(uuid);
        orderRepository.delete( order );
        return "deleted: "+ order.getUuid();
    }
    public void pay(){
        //TODO when an order was paid we delete the cart...i don't know in this moment
    }
}
