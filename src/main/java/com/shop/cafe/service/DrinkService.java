package com.shop.cafe.service;

import com.shop.cafe.domain.Drink;
import com.shop.cafe.repository.DrinkRepository;
import com.shop.cafe.service.mapper.DrinkMapper;
import com.shop.cafe.web.data.DrinkDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DrinkService {

    @Autowired
    DrinkRepository drinkRepository;
    private final DrinkMapper drinkMapper;

    public DrinkService(DrinkMapper drinkMapper) {
        this.drinkMapper = drinkMapper;
    }

    @Transactional
    public DrinkDto create(DrinkDto drinkDto) {
        //Drink
        Drink drink = drinkMapper.toDrink(drinkDto);
        drink.setUuid( UUID.randomUUID().toString() );
        //Save Drink
        Drink saveddrink = drinkRepository.save(drink);

        //Convert to DTO
        return drinkMapper.toDrinkDto(saveddrink);
    }

    public List<DrinkDto> getAll(Pageable pageable) {
        return drinkRepository.findAll(pageable).stream()
                .map( drinkMapper::toDrinkDto).collect( Collectors.toList());
    }

    public DrinkDto getByUuid(String uuid) {
        final Drink drink = drinkRepository.getByUuid(uuid);
        return drinkMapper.toDrinkDto( drink );
    }

    @Transactional
    public DrinkDto update(String uuid, DrinkDto drinkDto) {
        Drink drinkUpdated = drinkMapper.toDrinkUpdate(uuid, drinkDto);
        Drink savedDrink = drinkRepository.save( drinkUpdated );
        return drinkMapper.toDrinkDto(savedDrink);
    }

    public String delete(String uuid){
        Drink drink = drinkRepository.getByUuid(uuid);
        drinkRepository.delete( drink );
        return "deleted: "+ drink.getUuid();
    }

}
