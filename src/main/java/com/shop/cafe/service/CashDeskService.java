package com.shop.cafe.service;

import com.shop.cafe.domain.CashDesk;
import com.shop.cafe.service.mapper.CashDeskMapper;
import com.shop.cafe.web.data.CashDeskDto;
import com.shop.cafe.domain.Order;
import com.shop.cafe.repository.CashDeskRepository;
import com.shop.cafe.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CashDeskService {

    @Autowired
    private final CashDeskRepository cashDeskRepository;
    @Autowired
    private final OrderRepository orderRepository;
    @Autowired
    private final CashDeskMapper cashDeskMapper;


    @Transactional
    public CashDeskDto create(CashDeskDto cashDeskDto) {
        //CashDesk
        CashDesk cashDesk = cashDeskMapper.toCashDesk(cashDeskDto);
        cashDesk.setUuid( UUID.randomUUID().toString() );
        //Save CashDesk
        CashDesk savedcashDesk = cashDeskRepository.save(cashDesk);

        //Convert to DTO
        return cashDeskMapper.toCashDeskDto(savedcashDesk);
    }

    public CashDeskDto payOrder(CashDeskDto cashDeskDto, String orderUuid){
        Order order = orderRepository.getByUuid(orderUuid);
        cashDeskDto.setTotal( cashDeskDto.getTotal() + order.getTotalPrice().floatValue() );
        cashDeskRepository.save( cashDeskMapper.toCashDeskUpdate(cashDeskDto.getUuid(),cashDeskDto));
        return cashDeskDto;
    }

    public List<CashDeskDto> getAll(Pageable pageable) {
        return cashDeskRepository.findAll(pageable).stream()
                .map(cashDeskMapper::toCashDeskDto).collect( Collectors.toList());
    }

    public CashDeskDto getByUuid(String uuid) {
        final CashDesk cashDesk = cashDeskRepository.getByUuid(uuid);
        return cashDeskMapper.toCashDeskDto( cashDesk );
    }

    @Transactional
    public CashDeskDto update(String uuid, CashDeskDto cashDeskDto) {
        CashDesk cashDeskUpdated = cashDeskMapper.toCashDeskUpdate(uuid, cashDeskDto);
        CashDesk savedCashDesk = cashDeskRepository.save(cashDeskUpdated);
        return cashDeskMapper.toCashDeskDto(savedCashDesk);
    }

    public String delete(String uuid){
        CashDesk cashDesk = cashDeskRepository.getByUuid(uuid);
        cashDeskRepository.delete( cashDesk );
        return "deleted: "+ cashDesk.getUuid();
    }
}
