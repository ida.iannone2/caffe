package com.shop.cafe.service;

import com.shop.cafe.domain.CartItem;
import com.shop.cafe.web.data.CartDto;
import com.shop.cafe.web.data.CartItemDto;
import com.shop.cafe.domain.Cart;
import com.shop.cafe.repository.CartItemRepository;
import com.shop.cafe.repository.CartRepository;
import com.shop.cafe.service.mapper.CartMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
@Service
public class CartService {
    //included cartItemService
//included cartItemmapper
    @Autowired
    CartRepository cartRepository;
    @Autowired
    CartItemRepository cartItemRepository;
    private final CartMapper cartMapper;

    public CartService(CartMapper cartMapper) {
        this.cartMapper = cartMapper;
    }

    @Transactional
    public CartDto create(CartDto cartDto) {
        //Cart
        Cart cart = cartMapper.toNewCart(cartDto);
        //Save Cart
        Cart savedCart = cartRepository.save(cart);
        //Convert to DTO
        return cartMapper.toCartDto(savedCart);
    }

    public List<CartDto> getAll(Pageable pageable) {
        return cartRepository.findAll(pageable).stream()
                .map( cartMapper::toCartDto).collect( Collectors.toList());
    }

    public CartDto getByUuid(String uuid) {
        final Cart cart = cartRepository.getByUuid(uuid);
        return cartMapper.toCartDto( cart );
    }

    public CartItemDto getItemByUuid(String cartUuid, String itemUuid) {
        final Cart cart = cartRepository.getByUuid( cartUuid );
        final CartItem cartItem = cartItemRepository.getByUuid(itemUuid);
        return cartMapper.toCartItemDto(cartItem);
    }

    @Transactional
    public CartDto update(String uuid, CartDto cartDto) {
        Cart cartUpdated = cartMapper.toCartUpdate(uuid, cartDto);
        Cart savedCart = cartRepository.save( cartUpdated );
        return cartMapper.toCartDto(savedCart);
    }

    public String delete(String uuid){
        Cart cart = cartRepository.getByUuid(uuid);
        cartRepository.delete( cart );
        return "deleted: "+ cart.getUuid();
    }

    public CartDto addItem(CartItemDto cartItemDto) {
        CartItem cartItemWithUuid = cartMapper.toNewCartItem(cartItemDto);
        CartItem saved = cartItemRepository.save( cartItemWithUuid );
        Cart toReturn = cartRepository.save(  saved.getCart().addItem(saved) );
        return cartMapper.toCartDto(toReturn);
    }
    public CartDto addItemToCart(String uuidCart, CartItemDto cartItemDto) {
        Cart cart = cartRepository.getByUuid( uuidCart );
        CartItem cartItemWithUuid = cartMapper.toNewCartItem(cartItemDto);
        CartItem saved = cartItemRepository.save(cartItemWithUuid);
        Cart toReturn = cartRepository.save( cart.addItem(saved) );
        //TODO calculate new price for cart
        return cartMapper.toCartDto(toReturn);
    }
    public CartDto updateItemToCart(String uuidCart, String uuidItem, CartItemDto cartItemDto) {
        Cart cart = cartRepository.getByUuid( uuidCart );
        CartItem cartItem = cartItemRepository.getByUuid( uuidItem );
        CartItem cartItemUp = cartMapper.toCartItemUpdate(cart, cartItem, cartItemDto);

        CartItem saved = cartItemRepository.save( cartItemUp );
        Cart toReturn = cartRepository.save(  cart.addItem(saved) );
        return cartMapper.toCartDto(toReturn);
    }

    public CartDto deleteItem(String uuidCart, String uuidItem) {
        //deleteItemFromCart
        Cart cart = cartRepository.getByUuid( uuidCart );
        CartItem cartItem = cartItemRepository.getByUuid(uuidItem);
        cart.removeItem(cartItem);
        cartRepository.save( cart );
        return cartMapper.toCartDto(cart);
    }

    @Transactional
    public CartItemDto createItem(CartItemDto cartDto, String cartUuid) {
        //Cart
        CartItem cart = cartMapper.toNewCartItem(cartDto);
        //Save Cart
        CartItem savedcart = cartItemRepository.save(cart);
        //Convert to DTO
        return cartMapper.toCartItemDto(savedcart);
    }
    public CartItemDto createItemWithoutCart(CartItemDto cartDto) {
        //Cart
        CartItem cart = cartMapper.toCartItem(cartDto);
        //Save Cart
        CartItem savedcart = cartItemRepository.save(cart);
        //Convert to DTO
        return cartMapper.toCartItemDto(savedcart);
    }
}
