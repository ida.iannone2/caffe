package com.shop.cafe.service.component;

import com.shop.cafe.domain.Cart;
import com.shop.cafe.domain.CartItem;
import com.shop.cafe.domain.Order;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class OrderComponent {

    public BigDecimal calculateTotalPrice(Order order, Cart cart){
        //for every element in cart we get price,
        //the sum of prices and other value make final price
        Float total = cart.getTotalPrice().floatValue();
        for(CartItem ci: cart.getItems()){
            Float toAdd = ci.getPrice().floatValue();
            int qu= ci.getQuantity();
            total = total + (toAdd * qu);
        }
        total = total + cart.getCovered().floatValue();
        order.setTotalPrice(new BigDecimal( total ));

        //we have to add coverad and extra value to pay
        return order.getTotalPrice();
    }
}
