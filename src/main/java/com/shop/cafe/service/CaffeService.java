package com.shop.cafe.service;

import com.shop.cafe.domain.Caffe;
import com.shop.cafe.repository.CaffeRepository;
import com.shop.cafe.service.mapper.CaffeMapper;
import com.shop.cafe.web.data.CaffeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CaffeService {

    @Autowired
    CaffeRepository caffeRepository;
    private final CaffeMapper caffeMapper;

    public CaffeService(CaffeMapper caffeMapper) {
        this.caffeMapper = caffeMapper;
    }

    @Transactional
    public CaffeDto create(CaffeDto caffeDto) {
        //Caffe
        Caffe caffe = caffeMapper.toCaffe(caffeDto);
        //Save Caffe
        Caffe savedcaffe = caffeRepository.save(caffe);

        //Convert to DTO
        return caffeMapper.toCaffeDto(savedcaffe);
    }

    public List<CaffeDto> getAll(Pageable pageable) {
        return caffeRepository.findAll(pageable).stream()
                .map( caffeMapper::toCaffeDto).collect(Collectors.toList());
    }

    public CaffeDto getByUuid(String uuid) {
        final Caffe caffe = caffeRepository.getByUuid(uuid);
        return caffeMapper.toCaffeDto( caffe );
    }

    @Transactional
    public CaffeDto update(String uuid, CaffeDto postCaffeDto) {
        Caffe caffe = caffeRepository.getByUuid(uuid);
        caffe.setName( postCaffeDto.getName());
        Boolean active = postCaffeDto.getActive() != null
                ? postCaffeDto.getActive()
                : Boolean.TRUE;
        caffe.setActive(active);
        caffe.setAddress( postCaffeDto.getAddress());
        caffe.setContactName( postCaffeDto.getContactName());
        caffe.setContactPhone( postCaffeDto.getContactPhone());
        caffe.setContactEmail( postCaffeDto.getContactEmail());

        Caffe savedCaffe = caffeRepository.save( caffe );
        return caffeMapper.toCaffeDto(savedCaffe);
    }
    public String delete(String uuid){
        Caffe caffe = caffeRepository.getByUuid(uuid);
        caffeRepository.delete( caffe );
        return "deleted: "+ caffe.getUuid();
    }

}
