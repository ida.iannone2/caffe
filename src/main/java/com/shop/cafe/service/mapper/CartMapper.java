package com.shop.cafe.service.mapper;

import com.shop.cafe.domain.CartItem;
import com.shop.cafe.web.data.CartDto;
import com.shop.cafe.web.data.CartItemDto;
import com.shop.cafe.domain.Cart;
import com.shop.cafe.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.UUID;

@Service
public class CartMapper {
    @Autowired
    CartRepository cartRepository;

    public CartDto toCartDto(Cart cart) {
        CartDto cartDto = new CartDto();
        cartDto.setUuid(cart.getUuid());
        cartDto.setConsumerReference(cart.getConsumerReference() );
        cartDto.setCurrency(cart.getCurrency());
        cartDto.setTotalPrice(cart.getTotalPrice().floatValue());
        cartDto.setItems(cart.getItems());
        cartDto.setState(cart.getState());
        cartDto.setCovered(cart.getCovered().floatValue());
        return cartDto;
    }

    public Cart toCart(CartDto cartDto) {
        Cart cart = new Cart();
        cart.setUuid(cartDto.getUuid());
        cart.setState(cartDto.getState());
        cart.setConsumerReference(cart.getConsumerReference() );
        cart.setCurrency(cart.getCurrency());
        cart.setTotalPrice(cart.getTotalPrice());
        cart.setItems(cart.getItems());
        cart.setState(cart.getState());
        cart.setCovered(new BigDecimal( cartDto.getCovered()));
        return cart;
    }
    public Cart toNewCart(CartDto cartDto) {
        Cart cart = new Cart();
        cart.setUuid(UUID.randomUUID().toString());
        cart.setState(cartDto.getState());
        cart.setConsumerReference(cartDto.getConsumerReference() );
        cart.setCurrency(cartDto.getCurrency());
        cart.setTotalPrice(new BigDecimal(cartDto.getTotalPrice()));
        cart.setItems(cartDto.getItems());
        cart.setState(cartDto.getState());
        cart.setCovered(new BigDecimal( cartDto.getCovered()));
        return cart;
    }

    public Cart toCartUpdate(String uuid, CartDto cartDto) {
        Cart cart = new Cart();
        cart.setUuid(uuid);
        cart.setState(cartDto.getState());
        cart.setConsumerReference(cartDto.getConsumerReference() );
        cart.setCurrency(cartDto.getCurrency());
        cart.setTotalPrice(new BigDecimal( cartDto.getTotalPrice()));
        cart.setItems(cartDto.getItems());
        cart.setState(cartDto.getState());
        cart.setCovered(new BigDecimal( cartDto.getCovered()));
        return cart;
    }

    public CartItem toCartItem(CartItemDto cartItemDto) {
        CartItem cartItem = new CartItem();
        cartItem.setCart(cartRepository.getByUuid(cartItemDto.getUuid()));
        cartItem.setPrice( new BigDecimal(cartItemDto.getPrice()));
        cartItem.setQuantity(cartItemDto.getQuantity() );
        cartItem.setType(cartItemDto.getType());
        cartItem.setUuid(cartItemDto.getUuid());
        return cartItem;
    }
    public CartItem toNewCartItem(CartItemDto cartItemDto) {
        CartItem cartItem = new CartItem();
        cartItem.setCart(cartRepository.getByUuid(cartItemDto.getCartUuid()));
        cartItem.setPrice( new BigDecimal(cartItemDto.getPrice()));
        cartItem.setQuantity(cartItemDto.getQuantity() );
        cartItem.setType(cartItemDto.getType());
        cartItem.setUuid(UUID.randomUUID().toString());
        return cartItem;
    }

    public CartItemDto toCartItemDto(CartItem cartItem) {
        CartItemDto cartItemDto = new CartItemDto();
        cartItemDto.setCartUuid(cartItem.getCart().getUuid());
        cartItemDto.setPrice( cartItem.getPrice().floatValue());
        cartItemDto.setQuantity(cartItem.getQuantity() );
        cartItemDto.setType(cartItem.getType());
        cartItemDto.setUuid(cartItem.getUuid());
        return cartItemDto;
    }

    public CartItem toCartItemUpdate(Cart cart, CartItem cartItem, CartItemDto cartItemDto) {
        cartItem.setCart(cart);
        cartItem.setPrice( new BigDecimal(cartItemDto.getPrice()));
        cartItem.setQuantity(cartItemDto.getQuantity() );
        cartItem.setType(cartItemDto.getType());
        cartItem.setUuid(cartItemDto.getUuid());
        return cartItem;
    }
}
