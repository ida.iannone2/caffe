package com.shop.cafe.service.mapper;

import com.shop.cafe.domain.CashDesk;
import com.shop.cafe.web.data.CashDeskDto;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.UUID;

@Service
public class CashDeskMapper {
    public CashDeskDto toCashDeskDto(CashDesk cashDesk) {
        CashDeskDto cashDeskDto = new CashDeskDto();
        cashDeskDto.setUuid(cashDesk.getUuid());
        cashDeskDto.setName(cashDesk.getName());
        cashDeskDto.setTotal(cashDesk.getTotal().floatValue());
        cashDeskDto.setState(cashDesk.getState());
        return cashDeskDto;
    }

    public CashDesk toCashDesk(CashDeskDto cashDeskDto) {
        CashDesk cashDesk = new CashDesk();
        cashDesk.setUuid( UUID.randomUUID().toString());
        cashDesk.setName(cashDeskDto.getName());
        cashDesk.setTotal(new BigDecimal(cashDeskDto.getTotal()));
        cashDesk.setState(cashDeskDto.getState());
        return cashDesk;
    }

    public CashDesk toCashDeskUpdate(String uuid, CashDeskDto cashDeskDto) {
        CashDesk cashDesk = new CashDesk();
        cashDesk.setUuid(uuid);
        cashDesk.setName(cashDeskDto.getName());
        cashDesk.setTotal(new BigDecimal(cashDeskDto.getTotal()));
        cashDesk.setState(cashDeskDto.getState());
        return cashDesk;
    }
}
