package com.shop.cafe.service.mapper;

import com.shop.cafe.web.data.FoodDto;
import com.shop.cafe.domain.Food;
import org.springframework.stereotype.Service;

import java.util.UUID;
@Service
public class FoodMapper {

    public FoodDto toFoodDto(Food food) {
        FoodDto foodDto = new FoodDto();
        foodDto.setUuid(food.getUuid());
        foodDto.setName(food.getName());
        foodDto.setPrice(food.getPrice());
        foodDto.setType(food.getType());
        return foodDto;
    }

    public Food toFood(FoodDto foodDto) {
        Food food = new Food();
        food.setUuid(UUID.randomUUID().toString());
        food.setName(foodDto.getName());
        food.setPrice(foodDto.getPrice());
        food.setType(foodDto.getType());
        return food;
    }

    public Food toFoodUpdate(String uuid, FoodDto foodDto) {
        Food food = new Food();
        food.setUuid(uuid);
        food.setName(foodDto.getName());
        food.setPrice(foodDto.getPrice());
        food.setType(foodDto.getType());
        return food;
    }
}
