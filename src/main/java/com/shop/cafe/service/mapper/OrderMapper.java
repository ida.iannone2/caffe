package com.shop.cafe.service.mapper;

import com.shop.cafe.web.data.OrderDto;
import com.shop.cafe.domain.Order;
import com.shop.cafe.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class OrderMapper {
    @Autowired
    private CartRepository cartRepository;

    public OrderDto toOrderDto(Order order) {
        OrderDto orderDto = new OrderDto();
        orderDto.setUuid(order.getUuid());
        orderDto.setCartuuid(order.getCart().getUuid());
        orderDto.setDate(order.getDate());
        orderDto.setState(order.getState());
        orderDto.setTotalPrice( order.getTotalPrice());
        orderDto.setType(order.getType());
        return orderDto;
    }

    public Order toOrder(OrderDto orderDto) {
        Order order = new Order();
        order.setUuid(orderDto.getUuid());
        order.setCart(cartRepository.getByUuid(orderDto.getCartuuid()));
        order.setDate(orderDto.getDate());
        order.setState(orderDto.getState());
        order.setTotalPrice(orderDto.getTotalPrice());
        order.setType(orderDto.getType());
        return order;
    }
     public Order toNewOrder(OrderDto orderDto) {
            Order order = new Order();
            order.setUuid(UUID.randomUUID().toString());
            order.setCart(cartRepository.getByUuid(orderDto.getCartuuid()));
            order.setDate(orderDto.getDate());
            order.setState(orderDto.getState());
            order.setTotalPrice(orderDto.getTotalPrice());
            order.setType(orderDto.getType());
            return order;
        }

    public Order toOrderUpdate(String uuid, OrderDto orderDto) {
        Order order = new Order();
        order.setUuid(uuid);
        order.setCart(cartRepository.getByUuid(orderDto.getCartuuid()));
        order.setDate(orderDto.getDate());
        order.setState(orderDto.getState());
        order.setTotalPrice(orderDto.getTotalPrice());
        order.setType(orderDto.getType());
        return order;
    }
}
