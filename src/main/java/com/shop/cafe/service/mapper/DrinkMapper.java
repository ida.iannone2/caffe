package com.shop.cafe.service.mapper;

import com.shop.cafe.domain.Drink;
import com.shop.cafe.web.data.DrinkDto;
import org.springframework.stereotype.Service;

import java.util.UUID;
@Service
public class DrinkMapper {

    public DrinkDto toDrinkDto(Drink drink) {
        DrinkDto drinkDto = new DrinkDto();
        drinkDto.setUuid(drink.getUuid());
        drinkDto.setName(drink.getName());
        drinkDto.setPrice(drink.getPrice());
        drinkDto.setType(drink.getType());
        return drinkDto;
    }

    public Drink toDrink(DrinkDto drinkDto) {
        Drink drink = new Drink();
        drink.setUuid(UUID.randomUUID().toString());
        drink.setName(drinkDto.getName());
        drink.setPrice(drinkDto.getPrice());
        drink.setType(drinkDto.getType());
        return drink;
    }

    public Drink toDrinkUpdate(String uuid, DrinkDto drinkDto) {
        Drink drink = new Drink();
        drink.setUuid(uuid);
        drink.setName(drinkDto.getName());
        drink.setPrice(drinkDto.getPrice());
        drink.setType(drinkDto.getType());
        return drink;
    }
}
