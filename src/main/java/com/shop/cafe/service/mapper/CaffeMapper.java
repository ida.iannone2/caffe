package com.shop.cafe.service.mapper;

import com.shop.cafe.domain.Caffe;
import com.shop.cafe.repository.CaffeRepository;
import com.shop.cafe.web.data.CaffeDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CaffeMapper {
    @Autowired
    CaffeRepository caffeRepository;

    public Caffe toCaffe(CaffeDto caffeDto) {
        Caffe caffe = new Caffe();
        caffe.setUuid(UUID.randomUUID().toString());
        caffe.setActive(Boolean.TRUE);
        caffe.setAddress( caffeDto.getAddress() );
        caffe.setContactEmail( caffeDto.getContactEmail() );
        caffe.setName( caffeDto.getName() );
        caffe.setContactName( caffeDto.getContactName() );
        caffe.setContactPhone( caffeDto.getContactPhone() );
        return caffeRepository.save(caffe);
    }

    public CaffeDto toCaffeDto(Caffe caffe) {
        CaffeDto caffeDto = new CaffeDto();
        caffeDto.setActive( caffe.getActive() );
        caffeDto.setAddress( caffe.getAddress() );
        caffeDto.setContactEmail( caffe.getContactEmail() );
        caffeDto.setName( caffe.getName() );
        caffeDto.setUuid( caffe.getUuid() );
        caffeDto.setContactName( caffe.getContactName() );
        caffeDto.setContactPhone( caffe.getContactPhone() );
        return caffeDto;
    }
}
