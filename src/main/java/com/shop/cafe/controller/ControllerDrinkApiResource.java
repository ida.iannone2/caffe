package com.shop.cafe.controller;

import com.shop.cafe.service.DrinkService;
import com.shop.cafe.service.mapper.DrinkMapper;
import com.shop.cafe.web.data.DrinkDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Slf4j

public class ControllerDrinkApiResource {
        @Autowired
        private final DrinkMapper drinkMapper = new DrinkMapper();
        @Autowired
        private final DrinkService drinkService = new DrinkService(drinkMapper);

        @GetMapping(value = "/drinks/{uuid}", produces = "application/json")
        public DrinkDto getDrink (@PathVariable String uuid) {
            return drinkService.getByUuid(uuid);
        }
        @PostMapping(value = "/drinks")
        private DrinkDto  createDrink(@RequestBody DrinkDto drinkDto) {
            DrinkDto drink = drinkService.create(drinkDto);
           log.debug("DEBUG: "+drink.toString() );
            return drink;
        }

        @PutMapping(value = "/drinks/{uuid}", produces = "application/json")
        private DrinkDto updateDrink(@PathVariable String uuid, @RequestBody DrinkDto drinkDto) {
            return drinkService.update( uuid, drinkDto );
        }

        @DeleteMapping(value = "/drinks/{uuid}", produces = "application/json")
        private String deleteDrink(@PathVariable String uuid) {
            return drinkService.delete(uuid);
        }

}
