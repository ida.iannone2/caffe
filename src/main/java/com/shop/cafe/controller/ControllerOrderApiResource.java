package com.shop.cafe.controller;

import com.shop.cafe.service.OrderService;
import com.shop.cafe.service.mapper.OrderMapper;
import com.shop.cafe.web.data.OrderDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Slf4j
public class ControllerOrderApiResource {
    @Autowired
    private final OrderMapper orderMapper;
    @Autowired
    private final OrderService orderService;

    @GetMapping(value = "/orders/{uuid}", produces = "application/json")
    public OrderDto getOrder (@PathVariable String uuid) {
        return orderService.getByUuid(uuid);
    }

    @PostMapping(value = "/orders")
    private OrderDto  createOrder(@RequestBody OrderDto orderDto) {
        OrderDto order = orderService.create(orderDto);
        log.debug("DEBUG: "+order.toString() );
        return order;
    }

    @PutMapping(value = "/orders/{uuid}", produces = "application/json")
    private OrderDto updateOrder(@PathVariable String uuid, @RequestBody OrderDto orderDto) {
        return orderService.update( uuid, orderDto );
    }

    @DeleteMapping(value = "/orders/{uuid}", produces = "application/json")
    private String deleteOrder(@PathVariable String uuid) {
        return orderService.delete(uuid);
    }
}
