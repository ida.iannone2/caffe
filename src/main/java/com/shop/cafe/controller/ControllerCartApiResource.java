package com.shop.cafe.controller;

import com.shop.cafe.web.data.CartItemDto;
import com.shop.cafe.service.CartService;
import com.shop.cafe.service.mapper.CartMapper;
import com.shop.cafe.web.data.CartDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Slf4j
public class ControllerCartApiResource {
    @Autowired
    private final CartMapper cartMapper = new CartMapper();
    @Autowired
    private final CartService cartService = new CartService(cartMapper);

    @GetMapping(value = "/carts/{uuid}", produces = "application/json")
    public CartDto getCart (@PathVariable String uuid) {
        return cartService.getByUuid(uuid);
    }

    @GetMapping(value = "/carts/{cartUuid}/items/{uuidItem}", produces = "application/json")
    public CartItemDto getCartItem (@PathVariable String cartUuid, @PathVariable String  uuidItem) {
        return cartService.getItemByUuid(cartUuid, uuidItem);
    }
    @PostMapping(value = "/carts")
    public CartDto  createCart(@RequestBody CartDto cartDto) {
        CartDto cart = cartService.create(cartDto);
       log.debug( "DEBUG: "+cart.toString() );
        return cart;
    }
   /* @PostMapping(value = "/carts/items")
    private CartItemDto createCartItem(@RequestBody CartItemDto cartItemDto) {
        CartItemDto cartItem = cartService.createItemWithoutCart(cartItemDto);
        System.out.println( "DEBUG: "+cartItem.toString() );
        return cartItem;
    }*/
    @PutMapping(value = "/carts/{uuid}", produces = "application/json")
    private CartDto updateCart(@PathVariable String uuid, @RequestBody CartDto cartDto) {
        return cartService.update( uuid, cartDto );
    }

    @DeleteMapping(value = "/carts/{uuid}", produces = "application/json")
    private String deleteCart(@PathVariable String uuid) {
        return cartService.delete(uuid);
    }

    @PostMapping(value = "/carts/items", produces = "application/json")
    private CartDto createcartItem(@RequestBody CartItemDto cartItemDto) {
        return cartService.addItem(cartItemDto);
    }
    @PostMapping(value = "/carts/{uuid}/items", produces = "application/json")
    private CartDto addNewItemToCart(@PathVariable String uuid, @RequestBody CartItemDto cartItemDto) {
        return cartService.addItemToCart(uuid, cartItemDto);
    }
    @PutMapping(value = "/carts/{cartUuid}/items/{uuidItem}", produces = "application/json")
    private CartDto UpdateItemToCart(@PathVariable String cartUuid, @PathVariable String uuidItem,
                                  @RequestBody CartItemDto cartItemDto) {
        return cartService.updateItemToCart(cartUuid,uuidItem, cartItemDto);
    }
    @DeleteMapping(value = "/carts/{cartUuid}/items/{uuidItem}", produces = "application/json")
    private CartDto removeItem(@PathVariable String cartUuid, @PathVariable String uuidItem) {
        return cartService.deleteItem(cartUuid,uuidItem);
    }
}
