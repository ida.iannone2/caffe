package com.shop.cafe.controller;

import com.shop.cafe.service.FoodService;
import com.shop.cafe.service.mapper.FoodMapper;
import com.shop.cafe.web.data.FoodDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Slf4j

public class ControllerFoodApiResource {
    @Autowired
    private final FoodMapper foodMapper = new FoodMapper();
    @Autowired
    private final FoodService foodService = new FoodService(foodMapper);

    @GetMapping(value = "/foods/{uuid}", produces = "application/json")
    public FoodDto getFood (@PathVariable String uuid) {
        return foodService.getByUuid(uuid);
    }
    @PostMapping(value = "/foods")
    private FoodDto  createFood(@RequestBody FoodDto foodDto) {
        FoodDto food = foodService.create(foodDto);
        log.debug("DEBUG: "+food.toString() );
        return food;
    }

    @PutMapping(value = "/foods/{uuid}", produces = "application/json")
    private FoodDto updateFood(@PathVariable String uuid, @RequestBody FoodDto foodDto) {
        return foodService.update( uuid, foodDto );
    }

    @DeleteMapping(value = "/foods/{uuid}", produces = "application/json")
    private String deleteFood(@PathVariable String uuid) {
        return foodService.delete(uuid);
    }

}
