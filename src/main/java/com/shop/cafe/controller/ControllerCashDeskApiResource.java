package com.shop.cafe.controller;

import com.shop.cafe.service.CashDeskService;
import com.shop.cafe.web.data.CashDeskDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Slf4j
public class ControllerCashDeskApiResource {

    @Autowired
    private final CashDeskService cashdeskService;

    @GetMapping(value = "/cashdesks/{uuid}", produces = "application/json")
    public CashDeskDto getCashDesk (@PathVariable String uuid) {
        return cashdeskService.getByUuid(uuid);
    }
    @PostMapping(value = "/cashdesks")
    private CashDeskDto  createCashDesk(@RequestBody CashDeskDto cashdeskDto) {
        CashDeskDto cashdesk = cashdeskService.create(cashdeskDto);
        System.out.println( "DEBUG: "+cashdesk.toString() );
        return cashdesk;
    }
    @PostMapping(value = "/cashdesks/pay/{orderuuid}")
    private CashDeskDto payOrder(@PathVariable String orderuuid,
                                 @RequestBody CashDeskDto cashdeskDto) {
        CashDeskDto cashdesk = cashdeskService.payOrder(cashdeskDto, orderuuid);
       log.debug( "DEBUG: "+cashdesk.toString() );
        return cashdesk;
    }

    @PutMapping(value = "/cashdesks/{uuid}", produces = "application/json")
    private CashDeskDto updateCashDesk(@PathVariable String uuid,
                                       @RequestBody CashDeskDto cashdeskDto) {
        return cashdeskService.update( uuid, cashdeskDto );
    }

    @DeleteMapping(value = "/cashdesks/{uuid}", produces = "application/json")
    private String deleteCashDesk(@PathVariable String uuid) {
        return cashdeskService.delete(uuid);
    }
}
