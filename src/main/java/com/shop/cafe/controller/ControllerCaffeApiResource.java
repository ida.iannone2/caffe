package com.shop.cafe.controller;

import com.shop.cafe.service.CaffeService;
import com.shop.cafe.service.mapper.CaffeMapper;
import com.shop.cafe.web.data.CaffeDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController()
@RequestMapping("/api")
@RequiredArgsConstructor
@Slf4j
public class ControllerCaffeApiResource {
    @Autowired
    private final CaffeMapper caffeMapper = new CaffeMapper();
    @Autowired
    private final CaffeService caffeService = new CaffeService(caffeMapper);

    @GetMapping(value = "/caffe/{uuid}", produces = "application/json")
    public CaffeDto getCaffe (@PathVariable String uuid) {

        return caffeService.getByUuid(uuid);
    }
    @PostMapping(value = "/cafe")
    private CaffeDto  createCaffe(@RequestBody CaffeDto caffeDto) {
        CaffeDto caffe = caffeService.create(caffeDto);
        log.debug( "DEBUG: "+caffe.toString() );
       return caffe;
    }

    @PutMapping(value = "/caffe/{uuid}", produces = "application/json")
    private CaffeDto  updateCaffe(@PathVariable String uuid, @RequestBody CaffeDto caffeDto) {
       return caffeService.update( uuid, caffeDto );
    }

    @DeleteMapping(value = "/caffe/{uuid}", produces = "application/json")
    private String  deleteCaffe(@PathVariable String uuid) {
       return caffeService.delete(uuid);
    }
}
